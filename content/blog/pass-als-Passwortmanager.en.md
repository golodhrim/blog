+++
author = "Martin Scholz"
categories = ["Linux", "Security", "pass", "Passwortmanager", "DevSecOps"]
tags = ["Linux", "Security", "pass", "Passwortmanager", "DevSecOps"]
date = "2021-03-17"
description = "Pass als Passwortmanager in DevSecOps"
linktitle = "Pass als Passwortmanager in DevSecOps-Teams"
title = "pass als Passwortmanager"
type = "post"
[[images]]
  src = "img/blog/cyber-security.jpg"
  alt = "CyberSecurity Image"
  stretch = "horizontal"
+++

# pass im Team nutzen

pass, der "standard unix Passwortmanager", ist ein großartiges Tool, wenn man volle Kontrolle über seinen Passwortspeicher haben will und gleichzeit auf multiplen Systemen nutzen will. Informationen zu pass können unter [https://passwordstore.org](https://passwordstore.org "pass Homepage") gefunden werden. Leider wurde vor etwa 2 Jahren ein nativer Windows-Support eingestellt, jedoch soll mittels WSL auch pass unter Windows nutzbar sein.

Um pass im Team zu nutzen sind neben den generellen obigen Einstellungen auf der pass-Homepage weitere Schritte nötig. Hier soll aufgezeigt werden, was dazu notwendig ist und wie das vorgehen ist.

# Voraussetzungen

Es sollten pass, git und gpg installiert sein. Git sollte konfiguriert sein und es sollte ein Repository für pass verfügbar sein, worin die Passwörter dann abgelegt werden.

Es sollte für jedes Teammitglied ein GPG-Key existieren und beim Ersteller sollten die öffentlichen GPG-Schlüssel jedes Teammitglieds im Schlüsselbund abgelegt sein.

Im nachfolgenden Abschnitten wird dann folgendes angenommen:

Die Firma Beispiel GmbH unterhält ein DevSecOps Team welches sich selbst Mordor nennt. Seine Teammitglieder heißen Paul, Lea, Julia und Marvin. Paul ist im Team für die Security verantwortlich und richtet für alle einen gemeinsamen Passwortspeicher ein. Die Teammitglieder-GPG-Schlüssel sind über die eMails der Teammitglieder auffindbar:

-   Paul: [paul@beispiel.de](mailto:paul@beispiel.de)
-   Lea: [lea@beispiel.de](mailto:lea@beispiel.de)
-   Julia: [julia@beispiel.de](mailto:julia@beispiel.de)
-   Marvin: [marvin@beispiel.de](mailto:marvin@beispiel.de)

## Setup

Paul wird also den Passwortspeicher verwalten und verschlüsseln und dies so, das die anderen Teammitglieder ihn auch entschlüsseln können.

Lasst uns also einen leeren Passwortspeicher anlegen:

```shell
$ pass init paul@beispiel.de
```

Dies erzeugt den Passwortspeicher im Home-Verzeichnis des Nutzers. Alle Passwörter werden in Dateien und Ordnern innerhalb des Passwortspeichers organisiert. Ein geteilter Passwortspeicher sollte in seinem eigenem Unterordner im Passwortspeicher verwaltet werden. Dies kann mit der `-p` Option erzeugt werden:

```shell
$ pass init -p mordor paul@beispiel.de
```

Dieser Befehl erzeugt nun einen Unterordner mordor in dem .password-store Ordner.

Lasst uns nun ein Beispielpasswort anlegen:

```shell
$ pass generate mordor/sharedpass 23
```

Dies generiert ein neues Passwort im Unterordner mordor. Zum jetzigen Zeitpunkt kann aber nur Paul das Passwort entschlüsseln und somit auch lesen.

## Mehrere gpg-IDs nutzen

## Teilen mittels git

## Den Passwortstore aktualisieren

# Zusammenfassung
