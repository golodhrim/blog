---
title: Bewerben Mit LaTeX Golang Tool
date: 2021-06-23T12:21:26.000Z
author: Martin Scholz
categories: [Linux, Bewerbung, golang, LaTeX]
tags: [Linux, Bewerbung, golang, LaTeX]
description: golang tool für Bewerbungen mit LaTeX
linktitle: golang tool für Bewerbungen mit LaTeX
type: post
draft: false
images: [{src: img/blog/bewerbungen.jpg}]
---

Wer sich bewirbt wird immer wieder vor Herausforderungen gestellt. wie baue ich eine Bewerbung, wie wird sie Ansprechend um nur einige zu nennen.

Hierfür existiert von André Hilbig eine wundervolle Klasse für LaTeX, die es ermöglicht eine gesamte Bewerbungsmappe in einem Guss zu setzen. Alleinig das beiliegende Shell-Skrit war in seiner Ausführung etwas zu star für meine Wünsche, wodurch ich dieses in ein kleines golang-Tool umgewandelt habe und hier gerne der Allgemeinheit zur Verfügung stellen mag.

```golang
package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

    "gopkg.in/yaml.v2"

)

type Bewerber struct {
	Name                string `yaml:"name"`
	Vorname             string `yaml:"vorname"`
	Straße              string `yaml:"straße"`
	Plz                 string `yaml:"plz"`
	Stadt               string `yaml:"stadt"`
	Beruf               string `yaml:"beruf"`
	Email               string `yaml:"email"`
	Telefon             string `yaml:"telefon"`
	Mobil               string `yaml:"mobil"`
	Staatsangehörigkeit string `yaml:"staatsangehörigkeit"`
	Geburtsdatum        string `yaml:"geburtsdatum"`
}

type Anhang \[]struct {
	Anlage string `yaml:"anlage"`
}
type Config struct {
	Bewerber Bewerber `yaml:"bewerber"`
	Anhang   Anhang   `yaml:"anhang"`
}

func NewConfig(configPath string) (\*Config, error) {
	config := &Config{}

    file, err := os.Open(configPath)
    if err != nil {
    	return nil, err
    }
    defer file.Close()

    d := yaml.NewDecoder(file)

    if err := d.Decode(&config); err != nil {
    	return nil, err
    }

    return config, nil

}

var currentdate = time.Now()
var dateflag = flag.String("d", currentdate.Format("02.01.2006"), "Datumsstring im Format DD.MM.YYYY (default: "+currentdate.Format("02.01.2006")+")")
var firmaflag = flag.String("f", "Musterfirma", "Der Firmenname (default: Musterfirma)")
var anredeflag = flag.String("a", "Sehr geehrte Damen und Herren", "Anrede der Kontaktperson (default: Sehr geehrte Damen und Herren)")
var nameflag = flag.String("n", "", "Name der Kontaktperson(default: \\"\\")")
var strasseflag = flag.String("s", "Musterstraße 1", "Straße und Hausnummer der Firma (default: Musterstraße 1)")
var plzflag = flag.String("p", "12345", "PLZ der Firma (default: 12345)")
var ortflag = flag.String("o", "Musterhausen", "Ort der Firma (default: Musterhausen)")
var positionsflag = flag.String("b", "DevSecOps Engineer", "Bezeichnung der Stelle auf die sich beworben wird (default: DevSecOps Engineer)")
var rueckmeldungsflag = flag.String("r", "beworben", "Status der Bewerbung (default: beworben)")
var languageflag = flag.String("l", "de", "Sprachauswahlstring, wechsel zwischen deutscher (de), englischer (en) oder multilingualer (both) Bewerbungsvorlage (default: de)")
var configinc \[]string

func csvReader(csvPath string) ([][]string, error) {
	recordFile, err := os.Open(csvPath)
	if err != nil {
		return nil, err
	}
	defer recordFile.Close()

    reader := csv.NewReader(recordFile)

    records, _ := reader.ReadAll()

    return records, nil

}

func csvWriter(csvPath string, anschriftsarray [][]string) error {
	writeFile, err := os.Create(csvPath)
	if err != nil {
		return err
	}
	defer writeFile.Close()

    writer := csv.NewWriter(writeFile)

    err = writer.WriteAll(anschriftsarray)
    if err != nil {
    	return err
    }
    return nil

}

func createConfigContent(bcfg _Config, anschriften [][]string) \[]string {
	var anhang string = ""
	for \_, anlage := range bcfg.Anhang {
		anhang = anhang + fmt.Sprintf("%s", anlage)[1:len(fmt.Sprintf("%s", anlage))-1] + ".\\newline "
	}
	configinc = \[]string{
		"%%",
		"%% \\CharacterTable",
		"%%  {Upper-case    \\A\\B\\C\\D\\E\\F\\G\\H\\I\\J\\K\\L\\M\\N\\O\\P\\Q\\R\\S\\T\\U\\V\\W\\X\\Y\\Z",
		"%%   Lower-case    \\a\\b\\c\\d\\e\\f\\g\\h\\i\\j\\k\\l\\m\\n\\o\\p\\q\\r\\s\\t\\u\\v\\w\\x\\y\\z",
		"%%   Digits        \\0\\1\\2\\3\\4\\5\\6\\7\\8\\9",
		"%%   Exclamation   \\!     Double quote  \\\\"     Hash (number) \\#",
		"%%   Dollar        \\$     Percent       \\%     Ampersand     \\&",
		"%%   Acute accent  \\'     Left paren    \\(     Right paren   \\)",
		"%%   Asterisk      \\_     Plus          \\+     Comma         \\,",
		"%%   Minus         \\-     Point         \\.     Solidus       \\/",
		"%%   Colon         \\:     Semicolon     \\;     Less than     \\&lt;",
		"%%   Equals        \\=     Greater than  \\>     Question mark \\?",
		"%%   Commercial at \\@     Left bracket  \\[     Backslash     \\\\",
		"%%   Right bracket \\]     Circumflex    \\^     Underscore    \\_",
		"%%   Grave accent  \\\`     Left brace    \\{     Vertical bar  \\|",
		"%%   Right brace   \\}     Tilde         \\~}",
		"%% zusätzliche",
		"%% Farbdefinitionen:",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% Konfiguration der eigenen Daten                                                                 +",
		"%%                                                                                                 +",
		"\\Name{" + bcfg.Bewerber.Name + "}",
		"\\Vorname{" + bcfg.Bewerber.Vorname + "}",
		"\\Street{" + bcfg.Bewerber.Straße + "}",
		"\\Plz{" + bcfg.Bewerber.Plz + "}",
		"\\Stadt{" + bcfg.Bewerber.Stadt + "}",
		"\\MeinBeruf{" + bcfg.Bewerber.Beruf + "}",
		"\\EMail{" + bcfg.Bewerber.Email + "}",
		"%%\\Tel{" + bcfg.Bewerber.Telefon + "}",
		"\\Mobile{" + bcfg.Bewerber.Mobil + "}",
		"\\Sta{" + bcfg.Bewerber.Staatsangehörigkeit + "}",
		"\\GebDatum{" + bcfg.Bewerber.Geburtsdatum + "}",
		"%%                                                                                                 +",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% +----Achtung--------------------------------------+",
		"%% + ID meint (Zeilennummer-1) und nicht das Feld id +",
		"%% + Bsp: Eintrag in Zeile 48: ID 47                 +",
		"%% +                                                 +",
		"%% + Außer der Klasse ahilbig-bewerbung wird die     +",
		"%% + Option idPlain mitgegeben. Dann wird nach dem   +",
		"%% + exakten Match im Feld id gesucht.               +",
		"\\ID{" + strconv.Itoa(len(anschriften)-1) + "}",
		"%% +-------------------------------------------------+",
		"\\Anhang{" + anhang + "}{%",
	}
	for _, anlage := range bcfg.Anhang {
		configinc = append(configinc, "\\item "+fmt.Sprintf("%s", anlage)[1:len(fmt.Sprintf("%s", anlage))-1])
	}
	configinc = append(configinc, "}")
	configinc = append(configinc, "\\endinput")
	configinc = append(configinc, "%%")
	configinc = append(configinc, "%% End of file \`config.inc'.")

    return configinc

}

func main() {
	cfg, err := NewConfig("./config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	flag.Parse()
	var csvPath string
	csvPath = "./anschrift.csv"
	anschriften, err := csvReader(csvPath)
	if err != nil {
		fmt.Println("An error encountered ::", err)
	}
	ts, _ := time.Parse("02.01.2006", \*dateflag)
	_, week := ts.ISOWeek()
	var input = \[]string{strconv.Itoa(len(anschriften)), strconv.Itoa(week), _dateflag, _firmaflag, _anredeflag, _nameflag, _strasseflag, _plzflag, _ortflag, _positionsflag, _rueckmeldungsflag, ""}
	anschriften = append(anschriften, input)
	err = csvWriter(csvPath, anschriften)
	if err != nil {
		fmt.Println("An error encountered ::", err)
	}
	var foldername string = "bewerbung-" + strings.ReplaceAll(_firmaflag, " ", "_")
	err = os.Mkdir(foldername, 0755)
	if err != nil {
		fmt.Println("An error encountered ::", err)
	}
	var nbewerbung string = foldername + "/bewerbungsmappe-" + strings.ReplaceAll(\*firmaflag, " ", "_") + ".tex"
	var nbewerbungen string = foldername + "/bewerbungsmappe-" + strings.ReplaceAll(_firmaflag, " ", "\_") + "-en.tex"
	var original, errde = os.Open("bewerbung.tex")
	var originalen, erren = os.Open("bewerbung_en.tex")
	if _languageflag == "de" {
		original, errde = os.Open("bewerbung.tex")
	} else if _languageflag == "en" {
		original, erren = os.Open("bewerbung_en.tex")
	} else if _languageflag == "both" {
		original, errde = os.Open("bewerbung.tex")
		originalen, erren = os.Open("bewerbung_en.tex")
	} else {
		original, _ = os.Open("bewerbung.tex")
		fmt.Println("You selected the wrong language-flag, we used german (de)")
	}
	if errde != nil {
		fmt.Println("An error encountered ::", err)
	}
	if erren != nil {
		fmt.Println("An error encountered ::", erren)
	}
	defer original.Close()
	defer originalen.Close()
	if \*languageflag == "both" {
		new, err := os.Create(nbewerbung)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
		_, err = io.Copy(new, original)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
		newen, err := os.Create(nbewerbungen)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
		_, err = io.Copy(newen, originalen)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
	} else {
		new, err := os.Create(nbewerbung)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
		_, err = io.Copy(new, original)
		if err != nil {
			fmt.Println("An error encountered ::", err)
		}
	}
	new, err := os.Create(nbewerbung)
	if err != nil {
		fmt.Println("An error encountered ::", err)
	}
	_, err = io.Copy(new, original)
	if err != nil {
		fmt.Println("An error encountered ::", err)
	}

    configinc := createConfigContent(cfg, anschriften)

    f, err := os.Create(foldername + "/config.inc")
    if err != nil {
    	fmt.Println("An error encountered ::", err)
    }
    defer f.Close()
    configincwriter := bufio.NewWriter(f)
    for _, line := range configinc {
    	_, err := configincwriter.WriteString(line + "\n")
    	if err != nil {
    		fmt.Println("An error encountered ::", err)
    	}
    	err = configincwriter.Flush()
    	if err != nil {
    		fmt.Println("An error encountered ::", err)
    	}
    }

    var lanhang string = foldername + "/Anhang"
    var lfotoj string = foldername + "/Foto.jpg"
    var lfotop string = foldername + "/Foto.pdf"
    var lanschrift string = foldername + "/anschrift.csv"
    err = os.Symlink("../Anhang", lanhang)
    if err != nil {
    	fmt.Println("An error encountered ::", err)
    }
    err = os.Symlink("../Foto.jpg", lfotoj)
    if err != nil {
    	fmt.Println("An error encountered ::", err)
    }
    err = os.Symlink("../Foto.pdf", lfotop)
    if err != nil {
    	fmt.Println("An error encountered ::", err)
    }
    err = os.Symlink("../anschrift.csv", lanschrift)
    if err != nil {
    	fmt.Println("An error encountered ::", err)
    }

}
```

Weiterhin wird neben dem klassischen TeX-Layout das André Hilbig vorgab noch eine config.yaml benötigt, die dem folgendem Schema genügt:

```yaml
bewerber:
  name:                 Mustermann
  vorname:              Max
  straße:               Musterstraße 1
  plz:                  12345
  stadt:                Musterdorf
  beruf:                DevSecOps Engineer
  email:                max@mustermann.com
  telefon:              01234~567890
  mobil:                0123~4567890
  staatsangehörigkeit:  deutsch
  geburtsdatum:         01.01.2000

anhang:
  - anlage:             Ausbildungszeugnis
  - anlage:             Arbeitszeugnis
  - anlage:             Zertifikate
```

Der Quellcode, sowie alle notwendigen Dateien können hier in folgenden Formaten geladen werden:

[Download 7zip]

[Download zip]

[Download tar.xz]

[Download 7zip]: /dl/blog/bewerbung-golang-helper.7z

[Download zip]: /dl/blog/bewerbung-golang-helper.zip

[Download tar.xz]: /dl/blog/bewerbung-golang-helper.tar.xz
