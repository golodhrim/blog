+++
title = "Kontakt"
type = "contact"
netlify = false
emailservice = "formspree.io/scholz.m82@gmail.com"
contactname = "Ihr Name"
contactemail = "Ihr Emailadresse"
contactsubject = "Betreff"
contactmessage = "Ihre Nachricht"
contactlang = "de"
contactanswertime = 24
+++
